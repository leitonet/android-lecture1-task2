package com.example.task2;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Queue;

public class MainActivity extends AppCompatActivity {

    EditText editText;
    Button buttonClear;
    Button buttonRestore;
    ArrayDeque<String> array = new ArrayDeque<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editText = (EditText) findViewById(R.id.editText);

        buttonClear = (Button) findViewById(R.id.button_clear);
        buttonClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(array.size() == 5){
                    array.removeFirst();
                    array.offer(editText.getText().toString());
                    editText.setText(R.string.empty);
                }else {
                    array.offer(editText.getText().toString());
                    editText.setText(R.string.empty);
                }
            }
        });

        buttonRestore = (Button) findViewById(R.id.button_restore_text);
        buttonRestore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editText.setText(array.pollLast());
            }
        });

    }
}
